import React, {Component} from 'react';
import Video from 'react-native-video';
import {StyleSheet, Modal, View, Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import Sound from 'react-native-sound';
import {LogLevel, RNFFmpeg} from 'react-native-ffmpeg';
import RNFS from 'react-native-fs';

let soundRef = null;
export default class EditVideo extends Component {
  static navigationOptions = {
    title: 'Edit video',
    headerMode: 'none',
    headerShown: false,
  };

  state = {
    isModalVisible: false,
    isVideoPaused: false,
    selectedAudio: null,
    outputUri: null,
    isBeingInterlaced: false,
  };

  togglePlayPause = () => {
    const {isVideoPaused} = this.state;
    this.setState({
      isVideoPaused: !isVideoPaused,
    });
  };

  interlaceMusic = () => {
    const {navigation} = this.props;
    const {selectedAudio} = this.state;
    const videoUri = navigation.getParam('videoUri', null);
    const bundlePath = RNFS.MainBundlePath;
    const outputUri = `${RNFS.CachesDirectoryPath}/output.mov`;
    this.setState({
      outputUri: null,
      isBeingInterlaced: true,
    });
    this.destroySoundRef();

    // This might not work on Android
    RNFFmpeg.setFontDirectory('/System/Library/Fonts/Cache/');
    const ffCommand = `-y -i ${videoUri} -i ${bundlePath}/${selectedAudio} -shortest -c:a copy -filter_complex drawtext=text='rnt1':fontsize=100:fontcolor=yellow@0.6:x=100:y=H-100:fontfile="/System/Library/Fonts/CoreUI/SFUI.ttf" -preset ultrafast ${outputUri}`;
    RNFFmpeg.execute(ffCommand)
      .then(result => {
        this.setState({
          outputUri,
          isBeingInterlaced: false,
        });
        this.toggleModal();
        console.log('FFmpeg process exited with rc ', result);
      })
      .catch(err => {
        console.log('interlacing error', err);
      });
  };

  toggleModal = () => {
    const {isModalVisible} = this.state;
    this.setState({
      isModalVisible: !isModalVisible,
    });
    this.destroySoundRef();
  };

  destroySoundRef = () => {
    if (soundRef) {
      soundRef.stop();
      soundRef.release();
      soundRef = null;
    }
  };

  saveToGallery = () => {
    const {outputUri} = this.state;
    console.log(outputUri, RNFS.LibraryDirectoryPath);
    CameraRoll.saveToCameraRoll(outputUri);
    alert('Video saved successfully');
    // RNFS.copyFile(outputUri, `${RNFS.LibraryDirectoryPath}/myvideo.mov`)
    //   .then(res => {
    //     console.log(res);
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });
  };

  selectAndPlayMusic = musicFileName => {
    this.destroySoundRef();
    console.log(musicFileName);

    soundRef = new Sound(musicFileName, Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      // loaded successfully
      console.log(
        'duration in seconds: ' +
          soundRef.getDuration() +
          'number of channels: ' +
          soundRef.getNumberOfChannels(),
      );

      this.setState({
        selectedAudio: musicFileName,
      });
      // Play the sound with an onEnd callback
      soundRef.play(success => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    });
  };

  render() {
    const {navigation} = this.props;
    const {navigate} = navigation;
    const {isVideoPaused, isModalVisible, outputUri, isBeingInterlaced} = this.state;
    const videoUri = navigation.getParam('videoUri', null);
    return (
      <>
        <Video
          repeat
          source={{uri: outputUri || videoUri}}
          ref={videoRef => {
            this.player = videoRef;
          }}
          paused={isVideoPaused}
          onBuffer={this.onBuffer}
          onError={this.videoError}
          style={styles.backgroundVideo}
        />
        <View style={styles.rightControlPanel}>
          <TouchableOpacity
            style={styles.playPause}
            onPress={this.saveToGallery}>
            <Text>Save</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.playPause} onPress={this.toggleModal}>
            <Text>Music</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.playPause}
            onPress={this.togglePlayPause}>
            <Text>{isVideoPaused ? 'Play' : 'Pause'}</Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={isModalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{height: '100%'}}>
            {isBeingInterlaced ? (
              <View style={styles.processingView}>
                <ActivityIndicator size="large" style={styles.loader} />
                <Text>Processing your video</Text>
              </View>
            ) : (
              <View>
                <TouchableOpacity
                  style={styles.playPause}
                  onPress={() => {
                    this.selectAndPlayMusic('1111.mp3');
                  }}>
                  <Text>1111.mp3</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.playPause}
                  onPress={() => {
                    this.selectAndPlayMusic('2222.m4a');
                  }}>
                  <Text>2222.m4a</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.playPause}
                  onPress={this.interlaceMusic}>
                  <Text>Use this music</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.playPause}
                  onPress={() => {
                    this.toggleModal();
                  }}>
                  <Text>Hide Modal</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </Modal>
      </>
    );
  }
}

const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  rightControlPanel: {
    height: '100%',
    position: 'absolute',
    right: 0,
    justifyContent: 'flex-end',
  },
  loader: {
    marginBottom: 20,
  },
  processingView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  playPause: {
    flex: 0,
    backgroundColor: '#fff',
    borderColor: 'red',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});
