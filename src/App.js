import React, {Component} from 'react';
import {StatusBar} from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import CaptureVideo from './screens/capture-video';
import EditVideo from './screens/edit-video';

const MainNavigator = createStackNavigator({
  CaptureVideo,
  EditVideo,
});

const NavvedApp = createAppContainer(MainNavigator);

export default class App extends Component {
  render() {
    return (
      <>
        <StatusBar hidden />
        <NavvedApp />
      </>
    );
  }
}
